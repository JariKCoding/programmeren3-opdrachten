package be.kdg.distrib.stubFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class stub, String hostAddress, int portNumber) {
        InvocationHandler handler = new StubInvocationHandler(hostAddress, portNumber);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{ stub }, handler);
    }
}
