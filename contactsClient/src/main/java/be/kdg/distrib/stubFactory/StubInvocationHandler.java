package be.kdg.distrib.stubFactory;

import be.kdg.distrib.ObjectMapper;
import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

public class StubInvocationHandler implements InvocationHandler {
    private final String hostAddress;
    private final int portNumber;

    public StubInvocationHandler(String hostAddress, int portNumber) {
        this.hostAddress = hostAddress;
        this.portNumber = portNumber;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);

        MessageManager messageManager = new MessageManager();
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                System.out.println("\targ" + i + ": " + args[i]);
                if (args[i].getClass().isPrimitive() ||
                        args[i].getClass().equals(Integer.class) ||
                        args[i].getClass().equals(Double.class) ||
                        args[i].getClass().equals(Boolean.class) ||
                        args[i].getClass().equals(Character.class) ||
                        args[i].getClass().equals(String.class)) {
                    message.setParameter("arg" + i, args[i].toString());
                } else {
                    for (Field field : args[i].getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        message.setParameter("arg" + i + "." + field.getName(), field.get(args[i]).toString());
                    }
                }
            }
        }
        NetworkAddress networkAddress = new NetworkAddress(hostAddress, portNumber);
        messageManager.send(message, networkAddress);

        Class<?> returnType = method.getReturnType();
        if (returnType.isPrimitive()) {
            String reply = messageManager.wReceive().getParameter("result");
            if (returnType.equals(int.class)) return Integer.parseInt(reply);
            if (returnType.equals(char.class)) return reply.charAt(0);
            if (returnType.equals(boolean.class)) return Boolean.parseBoolean(reply);
            return reply;
        } else if (returnType.equals(String.class)) {
            return messageManager.wReceive().getParameter("result");
        } else {
            Map<String, String> objectValues = messageManager.wReceive().getParametersStartingWith("result.");
            return ObjectMapper.createObject(returnType, objectValues);
        }
    }
}
