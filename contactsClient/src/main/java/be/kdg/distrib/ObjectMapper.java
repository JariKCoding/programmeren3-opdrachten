package be.kdg.distrib;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class ObjectMapper {
    public static Object createObject(Class<?> newClass, Map<String, String> objectValues) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Object obj = newClass.getDeclaredConstructor().newInstance();
        for (Map.Entry<String, String> entry : objectValues.entrySet()) {
            String key = entry.getKey().contains(".") ? entry.getKey().split("\\.")[1] : entry.getKey();
            Field field = newClass.getDeclaredField(key);
            field.setAccessible(true);
            if (field.getType().equals(int.class)) field.set(obj, Integer.parseInt(entry.getValue()));
            if (field.getType().equals(char.class)) field.set(obj, entry.getValue().charAt(0));
            if (field.getType().equals(String.class)) field.set(obj, entry.getValue());
            if (field.getType().equals(boolean.class)) field.set(obj, Boolean.parseBoolean(entry.getValue()));
        }
        return obj;
    }
}
