package be.kdg.client;

import be.kdg.distrib.stubFactory.StubFactory;

public class Client {
    public static void main(String[] args) {
        String hostname = "127.0.0.1";
        int port = 58554;
        Contacts contacts = (Contacts) StubFactory.createStub(Contacts.class, hostname, port);

        Address address = new Address("Langestraat", "42", "2000", "Ergens");
        contacts.add("ikke", address, "03/123.45.67");
        address = new Address("Langestraat", "42", "2000", "Antwerpen");
        contacts.add("gij", address, "03/765.43.21");

        System.out.println("address of ikke is:");
        address = contacts.addressOf("ikke");
        System.out.println(address);
        System.out.println("address of gij is:");
        address = contacts.addressOf("gij");
        System.out.println(address);
        System.out.println("address of blah is:");
        address = contacts.addressOf("blah");
        System.out.println(address);
    }
}
