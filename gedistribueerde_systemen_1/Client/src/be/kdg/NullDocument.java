package be.kdg;


import be.kdg.util.Nullable;

public class NullDocument implements Nullable, Document {
    private static NullDocument theNullDocument = new NullDocument();

    public static Document getInstance() {
        return theNullDocument;
    }

    @Override
    public String getText() {
        return "Invalid document";
    }

    @Override
    public void setText(String text) {

    }

    @Override
    public void append(char c) {

    }

    @Override
    public void setChar(int position, char c) {

    }

    @Override
    public boolean isNull() {
        return false;
    }


}
