package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

public class ServerStub implements Server {
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public ServerStub(NetworkAddress serverAddress) {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    @Override
    public void log(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "log");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    @Override
    public Document create(String s) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "create");
        message.setParameter("text", s);
        messageManager.send(message, serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        if (!"result".equals(reply.getMethodName())) return NullDocument.getInstance();
        String text = reply.getParameter("result.text");
        return new DocumentImpl(text);
    }

    @Override
    public void toUpper(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toUpper");
        message.setParameter("text", document.getText());
        // ServerSkeleton skeleton = new ServerSkeleton(document);
        // message.setParameter("address", skeleton.getNetworkAddress().toString());
        messageManager.send(message, serverAddress);
        // messageManager.wReceive();
        MethodCallMessage reply = messageManager.wReceive();
        if (!"result".equals(reply.getMethodName())) {
            document.setText(NullDocument.getInstance().getText());
        }
        String text = reply.getParameter("result.text");
        document.setText(text);
    }

    @Override
    public void toLower(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toLower");
        message.setParameter("text", document.getText());
        // ServerSkeleton skeleton = new ServerSkeleton(document);
        // message.setParameter("address", skeleton.getNetworkAddress().toString());
        messageManager.send(message, serverAddress);
        // messageManager.wReceive();
        MethodCallMessage reply = messageManager.wReceive();
        if (!"result".equals(reply.getMethodName())) {
            document.setText(NullDocument.getInstance().getText());
        }
        String text = reply.getParameter("result.text");
        document.setText(text);
    }

    @Override
    public void type(Document document, String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "type");
        message.setParameter("text", text);
        ServerSkeleton skeleton = ServerSkeletonFactory.getStubForDocument(document);
        message.setParameter("address", skeleton.getNetworkAddress().toString());
        messageManager.send(message, serverAddress);
        messageManager.wReceive();
    }
}
