package be.kdg;

import java.util.*;

public class ServerSkeletonFactory {
    private static List<ServerSkeleton> serverSkeletons = new ArrayList<>();


    public static ServerSkeleton getStubForDocument(Document document) {
        Optional<ServerSkeleton> optServerSkeleton = serverSkeletons.stream().filter(ss -> ss.getDocument() != document).findFirst();
        return optServerSkeleton.orElseGet(() -> new ServerSkeleton(document));
    }
}
