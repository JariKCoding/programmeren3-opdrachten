package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

import javax.print.Doc;

public class ServerSkeleton {
    private final Document document;
    private final MessageManager messageManager;

    public ServerSkeleton(Document document) {
        this.messageManager = new MessageManager();
        this.document = document;

        new Thread(this::run).start();
    }

    public Document getDocument() {
        return document;
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    public NetworkAddress getNetworkAddress() {
        return messageManager.getMyAddress();
    }

    private void handleAppend(MethodCallMessage request) {
        char c = request.getParameter("char").charAt(0);
        document.append(c);
        sendEmptyReply(request);
    }

    private void handleSetText(MethodCallMessage request) {
        String text = request.getParameter("text");
        document.setText(text);
        sendEmptyReply(request);
    }

    private void handleSetChar(MethodCallMessage request) {
        int position = Integer.parseInt(request.getParameter("position"));
        char c = request.getParameter("char").charAt(0);
        document.setChar(position, c);
        sendEmptyReply(request);
    }

    private void handleRequest(MethodCallMessage request) {
        System.out.println("ServerSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("append".equals(methodName)) {
            handleAppend(request);
        } else if ("setText".equals(methodName)) {
            handleSetText(request);
        } else if ("setChar".equals(methodName)) {
            handleSetChar(request);
        } else {
            System.out.println("ServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}
