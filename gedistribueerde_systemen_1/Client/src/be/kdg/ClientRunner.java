package be.kdg;

import be.kdg.communication.NetworkAddress;

public class ClientRunner {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java Client <contactsIP> <contactsPort>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAddress = new NetworkAddress(args[0], port);
        Client client = new Client(new ServerStub(serverAddress), new DocumentImpl());
        client.run();
    }
}
