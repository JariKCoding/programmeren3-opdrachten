package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private final Server server;

    public ServerSkeleton() {
        this.messageManager = new MessageManager();
        this.server = new ServerImpl();
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleLog(MethodCallMessage request) {
        String text = request.getParameter("text");
        Document document = new DocumentImpl(text);
        server.log(document);
        sendEmptyReply(request);
    }

    private void handleCreate(MethodCallMessage request) {
        String text = request.getParameter("text");
        Document document = server.create(text);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result.text", document.getText());
        messageManager.send(reply, request.getOriginator());
    }

    private void handleToUpper(MethodCallMessage request) {
        String text = request.getParameter("text");
        DocumentImpl document = new DocumentImpl(text);
        server.toUpper(document);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result.text", document.getText());
        messageManager.send(reply, request.getOriginator());
    }

    private void handleToLower(MethodCallMessage request) {
        String text = request.getParameter("text");
        DocumentImpl document = new DocumentImpl(text);
        server.toLower(document);
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result.text", document.getText());
        messageManager.send(reply, request.getOriginator());
    }

    private void handleType(MethodCallMessage request) {
        String text = request.getParameter("text");
        String address = request.getParameter("address");
        DocumentStub document = new DocumentStub(messageManager, new NetworkAddress(address.split(":")[0], Integer.parseInt(address.split(":")[1])));
        server.type(document, text);
        sendEmptyReply(request);
    }

    private void handleRequest(MethodCallMessage request) {
        System.out.println("ServerSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("log".equals(methodName)) {
            handleLog(request);
        } else if ("create".equals(methodName)) {
            handleCreate(request);
        } else if ("toUpper".equals(methodName)) {
            handleToUpper(request);
        } else if ("toLower".equals(methodName)) {
            handleToLower(request);
        } else if ("type".equals(methodName)) {
            handleType(request);
        } else if ("result".equals(methodName)) {

        } else {
            System.out.println("ServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    public static void main(String[] args) {
        ServerSkeleton documentSkeleton = new ServerSkeleton();
        System.out.println(documentSkeleton.messageManager.getMyAddress());
        documentSkeleton.run();
    }
}
