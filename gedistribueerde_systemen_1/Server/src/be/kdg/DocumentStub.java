package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

import java.awt.event.TextEvent;
import java.awt.event.TextListener;

public class DocumentStub implements Document {
    private StringBuilder text;
    private final MessageManager messageManager;
    private final NetworkAddress ipAddress;

    public DocumentStub(MessageManager messageManager, NetworkAddress ipAddress) {
        this(messageManager, ipAddress, "");
    }

    public DocumentStub(MessageManager messageManager, NetworkAddress ipAddress, String text) {
        this.messageManager = messageManager;
        this.ipAddress = ipAddress;
        this.text = new StringBuilder(text);
    }

    @Override
    public String getText() {
        return this.text.toString();
    }

    @Override
    public void setText(String text) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "setText");
        reply.setParameter("text", text);
        messageManager.send(reply, ipAddress);
    }

    @Override
    public void append(char c) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "append");
        reply.setParameter("char", String.valueOf(c));
        messageManager.send(reply, ipAddress);
    }

    @Override
    public void setChar(int position, char c) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "setChar");
        reply.setParameter("position", String.valueOf(position));
        reply.setParameter("char", String.valueOf(c));
        messageManager.send(reply, ipAddress);
    }
}
