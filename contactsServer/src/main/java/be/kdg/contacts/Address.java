package be.kdg.contacts;

public class Address {
    private final String street;
    private final String number;
    private final String zip;
    private final String city;
    private final boolean nullboolean;

    public Address() {
        this.street = "Invalid Street";
        this.number = "Invalid Number";
        this.zip = "Invalid Zip";
        this.city = "Invalid City";
        nullboolean = true;
    }

    /**
     * Constructs a new Address with the given street, number, zip-code and city.
     *
     * @param street the name of the street.
     * @param number the number (may contain any characters).
     * @param zip    the zip-code of the city.
     * @param city   the name of the city.
     */
    public Address(String street, String number, String zip, String city) {
        this.street = street;
        this.number = number;
        this.zip = zip;
        this.city = city;
        this.nullboolean = false;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

    public String toString() {
        return street + " " + number + ", " + zip + " " + city;
    }

    public boolean isNull() {
        return nullboolean;
    }
}
