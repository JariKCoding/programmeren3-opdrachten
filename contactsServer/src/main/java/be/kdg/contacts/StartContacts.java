package be.kdg.contacts;

import be.kdg.distrib.skeletonFactory.Skeleton;
import be.kdg.distrib.skeletonFactory.SkeletonFactory;

public class StartContacts {
    public static void main(String[] args) {
        Contacts contacts = new ContactsImpl();
        Skeleton skeleton = (Skeleton) SkeletonFactory.createSkeleton(contacts);
        System.out.println("skeleton.getAddress() = " + skeleton.getAddress());
        skeleton.run();
    }
}
