package be.kdg.contacts;

import java.util.Map;
import java.util.TreeMap;

/**
 * Implementation of the Contacts component.
 */
public final class ContactsImpl implements Contacts {
    private final Map<String, Contact> contacts;

    /**
     * Creates a new Contacts component.
     */
    public ContactsImpl() {
        contacts = new TreeMap<String, Contact>();
    }

    /**
     * @see Contacts#add
     */
    public void add(String name, Address address, String tel) {
        System.out.println("ContactsImpl:add(" + name + ", " + address + ", " + tel + ")");
        Contact contact = new Contact(name, address, tel);
        contacts.put(name, contact);
    }

    /**
     * @see Contacts#addressOf
     */
    public Address addressOf(String name) {
        //System.out.println("ContactsImpl:addressOf(" + name + ")");
        Contact contact = contacts.get(name);
        if (contact == null) {
            return new Address();
        }
        return contact.getAddress();
    }

    /**
     * @see Contacts#remove
     */
    public void remove(String name) {
        //System.out.println("ContactsImpl:remove(" + name + ")");
        contacts.remove(name);
    }
}
