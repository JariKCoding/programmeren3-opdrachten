package be.kdg.distrib.skeletonFactory;

public class SkeletonFactory {
    public static Object createSkeleton(Object implementation) {
        return new SkeletonImplementation(implementation);
    }
}
