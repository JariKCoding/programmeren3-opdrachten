package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.ObjectMapper;
import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class SkeletonImplementation implements Skeleton {
    private final MessageManager messageManager;
    private final Object implementation;

    public SkeletonImplementation(Object implementation) {
        this.implementation = implementation;
        this.messageManager = new MessageManager();
    }

    @Override
    public void run() {
        new Thread(() -> {
            while (true) {
                MethodCallMessage request = messageManager.wReceive();
                handleRequest(request);
            }
        }).start();
    }

    @Override
    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    @Override
    public void handleRequest(MethodCallMessage request) {
        // Get the method
        Optional<Method> optMethod = Arrays.stream(implementation.getClass().getDeclaredMethods()).filter(m -> m.getName().equals(request.getMethodName())).findFirst();
        if (optMethod.isEmpty()) {
            throw new RuntimeException("Method not found");
        }
        Method implMethod = optMethod.get();

        // Check for the correct given parameters
        for (Map.Entry<String, String> param : request.getParameters().entrySet()) {
            if (!param.getKey().startsWith("arg")) {
                throw new RuntimeException("Invalid parameter name");
            }
        }

        // Check for incorrect parameter count
        long count = request.getParameters().keySet().stream().map(k -> k.contains(".") ? k.split("\\.")[0] : k).distinct().count();
        if (count != implMethod.getParameterCount()) {
            throw new RuntimeException("Invalid parameter count");
        }

        try {
            Object[] paramsArray = createParameterArray(implMethod, request);
            Object returnValue = implMethod.invoke(implementation, paramsArray);
            sendResponse(implMethod, returnValue, request.getOriginator());
        } catch (InvocationTargetException | IllegalAccessException | InstantiationException | NoSuchFieldException | NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private Object[] createParameterArray(Method implMethod, MethodCallMessage request) throws InvocationTargetException, NoSuchMethodException, NoSuchFieldException, InstantiationException, IllegalAccessException {
        Object[] paramsArray = new Object[implMethod.getParameterCount()];

        for(int i = 0; i < implMethod.getParameterCount(); i++) {
            Class<?> parameterClass = implMethod.getParameterTypes()[i];
            if (parameterClass.isPrimitive()) {
                String param = request.getParameter("arg" + i);
                if (parameterClass.equals(int.class)) paramsArray[i] = Integer.parseInt(param);
                if (parameterClass.equals(double.class)) paramsArray[i] = Double.parseDouble(param);
                if (parameterClass.equals(boolean.class)) paramsArray[i] = Boolean.parseBoolean(param);
                if (parameterClass.equals(char.class)) paramsArray[i] = param.charAt(0);
            } else if (parameterClass.equals(String.class)) {
                paramsArray[i] = request.getParameter("arg" + i);
            } else {
                Map<String, String> objectValues = request.getParametersStartingWith("arg" + i);
                paramsArray[i] = ObjectMapper.createObject(parameterClass, objectValues);
            }
        }

        return paramsArray;
    }

    private void sendResponse(Method implMethod, Object returnValue, NetworkAddress originator) throws IllegalAccessException {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        Class<?> returnType = implMethod.getReturnType();
        if (returnType.equals(Void.TYPE)) {
            reply.setParameter("result", "Ok");
        } else if (returnType.isPrimitive() || returnType.equals(String.class)) {
            reply.setParameter("result", returnValue.toString());
        } else {
            for (Field field : returnValue.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                reply.setParameter("result." + field.getName(), field.get(returnValue).toString());
            }
        }
        messageManager.send(reply, originator);
    }
}
